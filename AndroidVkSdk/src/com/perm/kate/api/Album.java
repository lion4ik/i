package com.perm.kate.api;

import com.google.api.client.util.Key;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Album implements Serializable {

    public Album(){}


    @Key
    private long aid;

    @Key
    private long thumb_id;

    @Key
    private long owner_id;

    @Key
    private String title;

    @Key
    private String description;

    @Key
    private long created;

    @Key
    private long updated;

    @Key
    private long size;

    @Key
    private long privacy;

    @Key
    private long comment_privacy;

    @Key
    private String thumb_src;

    @Key
    private List<Size> sizes;

    public String getBestThumbForPoster(){
        for(Size size : sizes){
            if(size!=null)
                return size.src;
        }
        return getThumb_src();
    }

    public static Album parse(JSONObject o) throws JSONException {
        Album a = new Album();
        a.title = Api.unescape(o.optString("title"));
        a.aid = Long.parseLong(o.getString("id"));
        a.owner_id = Long.parseLong(o.getString("owner_id"));
        String description = o.optString("description");
        if (description != null && !description.equals("") && !description.equals("null"))
            a.description = Api.unescape(description);
        String thumb_id = o.optString("thumb_id");
        if (thumb_id != null && !thumb_id.equals("") && !thumb_id.equals("null"))
            a.thumb_id = Long.parseLong(thumb_id);
        String created = o.optString("created");
        if (created != null && !created.equals("") && !created.equals("null"))
            a.created = Long.parseLong(created);
        
        JSONObject privacy=o.optJSONObject("privacy_view");
        if(privacy!=null){
            String type = privacy.optString("type");
            if("all".equals(type))
                a.privacy=0;
            else if("friends".equals(type))
                a.privacy=1;
            else if("friends_of_friends".equals(type))
                a.privacy=2;
            else if("nobody".equals(type))
                a.privacy=3;
            else if("users".equals(type))
                a.privacy=4;
        }
        
        JSONObject privacy_comment=o.optJSONObject("privacy_comment");
        if(privacy_comment!=null){
            String type = privacy_comment.optString("type");
            if("all".equals(type))
                a.comment_privacy=0;
            else if("friends".equals(type))
                a.comment_privacy=1;
            else if("friends_of_friends".equals(type))
                a.comment_privacy=2;
            else if("nobody".equals(type))
                a.comment_privacy=3;
            else if("users".equals(type))
                a.comment_privacy=4;
        }
        
        a.size = o.optLong("size");
        String updated = o.optString("updated");
        if (updated != null && !updated.equals("") && !updated.equals("null"))
            a.updated = Long.parseLong(updated);
        a.thumb_src = o.optString("thumb_src");
        JSONArray thumb_array = o.optJSONArray("sizes");
        a.sizes = new ArrayList<Size>();

        for(int i = 0; i < 10; i++)
            a.sizes.add(null);

        for(int i = 0; i < thumb_array.length(); i++){
            JSONObject thumb_obj = thumb_array.getJSONObject(i);

            switch (thumb_obj.getString("type")) {
                case "p":
                    a.sizes.set(0, new Size(thumb_obj.getString("src"), thumb_obj.getInt("width"), thumb_obj.getInt("height"),thumb_obj.getString("type")));
                    break;
                case "o":
                    a.sizes.set(1, new Size(thumb_obj.getString("src"), thumb_obj.getInt("width"), thumb_obj.getInt("height"),thumb_obj.getString("type")));
                    break;
                case "m":
                    a.sizes.set(2, new Size(thumb_obj.getString("src"), thumb_obj.getInt("width"), thumb_obj.getInt("height"),thumb_obj.getString("type")));
                    break;
                case "s":
                    a.sizes.set(3, new Size(thumb_obj.getString("src"), thumb_obj.getInt("width"), thumb_obj.getInt("height"),thumb_obj.getString("type")));
                    break;
                case "q":
                    a.sizes.set(4, new Size(thumb_obj.getString("src"), thumb_obj.getInt("width"), thumb_obj.getInt("height"),thumb_obj.getString("type")));
                    break;
                case "r":
                    a.sizes.set(5, new Size(thumb_obj.getString("src"), thumb_obj.getInt("width"), thumb_obj.getInt("height"),thumb_obj.getString("type")));
                    break;
                case "x":
                    a.sizes.set(6, new Size(thumb_obj.getString("src"), thumb_obj.getInt("width"), thumb_obj.getInt("height"),thumb_obj.getString("type")));
                    break;
                case "y":
                    a.sizes.set(7, new Size(thumb_obj.getString("src"), thumb_obj.getInt("width"), thumb_obj.getInt("height"),thumb_obj.getString("type")));
                    break;
                case "z":
                    a.sizes.set(8, new Size(thumb_obj.getString("src"), thumb_obj.getInt("width"), thumb_obj.getInt("height"),thumb_obj.getString("type")));
                    break;
                case "w":
                    a.sizes.set(9, new Size(thumb_obj.getString("src"), thumb_obj.getInt("width"), thumb_obj.getInt("height"),thumb_obj.getString("type")));
                    break;


            }
        }
        return a;
    }
    
    public static Album parseFromAttachment(JSONObject o) throws JSONException {
        Album a = new Album();
        a.title = Api.unescape(o.optString("title"));
        a.aid = Long.parseLong(o.getString("id"));
        a.owner_id = Long.parseLong(o.getString("owner_id"));
        return a;
    }

    public long getAid() {
        return aid;
    }

    public long getThumb_id() {
        return thumb_id;
    }

    public long getOwner_id() {
        return owner_id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public long getCreated() {
        return created;
    }

    public long getUpdated() {
        return updated;
    }

    public long getSize() {
        return size;
    }

    public long getPrivacy() {
        return privacy;
    }

    public long getComment_privacy() {
        return comment_privacy;
    }

    public String getThumb_src() {
        return thumb_src;
    }

    public List<Size> getSizes() {
        return sizes;
    }

    public static class Size{

        public Size(){}

        public Size(String src, Integer w, Integer h, String type){
            this.src = src;
            this.width = w;
            this.height = h;
            this.type = type;
        }

        @Key
        private String src;

        @Key
        private Integer width;

        @Key
        private Integer height;

        @Key
        private String type;
    }
}