package org.alexey.ifree;

import android.content.Context;
import android.view.View;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by alexey on 20.12.15.
 */
public class Utils {

    public static MaterialDialog createSimpleDialog(String title, String errorMsg, Context context){
        final MaterialDialog dialog = new MaterialDialog(context)
                .setCanceledOnTouchOutside(true)
                .setTitle(title)
                .setMessage(errorMsg);

        dialog.setPositiveButton(android.R.string.ok, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        return dialog;
    }
}
