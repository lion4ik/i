package org.alexey.ifree;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import org.alexey.ifree.view.CircleTransformation;

/**
 * Created by alexey on 16.12.15.
 */
public class DataBindingUtil {

    private DataBindingUtil(){}

    @BindingAdapter("imageUrl")
    public static void setImageUrl(ImageView imageView, String url) {
        Context context = imageView.getContext();
        Glide.with(context).load(url).into(imageView);
    }

    @BindingAdapter("imageUrlRounded")
    public static void setImageUrlRounded(ImageView imageView, String url) {
        Context context = imageView.getContext();
        Glide.with(context).load(url)
                .transform(new CircleTransformation(context))
                .crossFade()
                .into(imageView);
    }
}
