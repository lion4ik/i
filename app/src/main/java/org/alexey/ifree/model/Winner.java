package org.alexey.ifree.model;

import com.google.api.client.util.Key;

/**
 * Created by alexey on 17.12.15.
 */
public class Winner {

    @Key
    private String post_id;

    @Key
    private Integer vk_user_id;

    @Key
    private String first_name;

    @Key
    private String last_name;

    @Key
    private String photo_url;

    @Key
    private String prize_name;

    @Key
    private String bussines_name;

    @Key
    private String city;

    @Key
    private String date;

    public String getPostId() {
        return post_id;
    }

    public String getCity() {
        return city;
    }

    public String getPhotoUrl() {
        return photo_url;
    }

    public String getFirstName() {
        return first_name;
    }

    public Integer getVkUserId() {
        return vk_user_id;
    }

    public String getLastName() {
        return last_name;
    }

    public String getPrizeName() {
        return prize_name;
    }

    public String getBussinesName() {
        return bussines_name;
    }

    public String getDate() {
        return date;
    }
}
