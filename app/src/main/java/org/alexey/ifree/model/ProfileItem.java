package org.alexey.ifree.model;

import android.graphics.drawable.Drawable;

/**
 * Created by alexey on 20.12.15.
 */
public class ProfileItem {

    private String mText;
    private Drawable mIconRes;
    private String mTitle;

    public ProfileItem(String text, String title, Drawable iconRes){
        mText = text;
        mTitle = title;
        mIconRes = iconRes;
    }

    public Drawable getIconRes() {
        return mIconRes;
    }

    public String getText() {
        return mText;
    }
    public String getTitle(){
        return mTitle;
    }
}
