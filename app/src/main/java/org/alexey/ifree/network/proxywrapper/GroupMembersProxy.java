package org.alexey.ifree.network.proxywrapper;


import com.perm.kate.api.Group;

import java.util.ArrayList;

/**
 * Created by alexey on 21.12.15.
 */
public class GroupMembersProxy {

    private ArrayList<Group> mItems;

    public GroupMembersProxy(ArrayList<Group> items){
        mItems = items;
    }

    public ArrayList<Group> getItems(){
        return mItems;
    }
}
