package org.alexey.ifree.network.request;

import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

import org.alexey.ifree.App;
import org.alexey.ifree.network.proxywrapper.GroupJoinProxy;

/**
 * Created by alexey on 21.12.15.
 */
public class GroupJoinRequest extends GoogleHttpClientSpiceRequest<String> {
    private long mId;
    public GroupJoinRequest(long id) {
        super(String.class);
        mId = id;
        setRetryPolicy(null);
    }

    @Override
    public String loadDataFromNetwork() throws Exception {
        return App.getVKApi().joinGroup(mId,null,null);
    }
}
