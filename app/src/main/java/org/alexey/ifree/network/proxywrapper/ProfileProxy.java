package org.alexey.ifree.network.proxywrapper;

import com.perm.kate.api.User;

import java.util.ArrayList;

/**
 * Created by alexey on 20.12.15.
 */
public class ProfileProxy {

    private ArrayList<User> mItems;

    public ProfileProxy(ArrayList<User> items){
        mItems = items;
    }

    public ArrayList<User> getItems(){
        return mItems;
    }
}
