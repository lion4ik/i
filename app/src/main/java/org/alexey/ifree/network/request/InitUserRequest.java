package org.alexey.ifree.network.request;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

import org.alexey.ifree.network.proxywrapper.InitUserProxy;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by alexey on 20.12.15.
 */
public class InitUserRequest extends GoogleHttpClientSpiceRequest<InitUserProxy> {
    private Map<String, String> mParams;

    public InitUserRequest(Map<String, String> params) {
        super(InitUserProxy.class);
        setRetryPolicy(null);
        mParams = params;
    }

    @Override
    public InitUserProxy loadDataFromNetwork() throws Exception {
        GenericUrl url = new GenericUrl("http://XN--80AAAH7AN2E2D.XN--P1AI");
        JacksonFactory factory = new JacksonFactory();
        JsonHttpContent content = new JsonHttpContent(factory,mParams);
        url.setRawPath("/init.php");
        HttpRequest request = getHttpRequestFactory()
                .buildPostRequest(url, content);

        request.setParser(factory.createJsonObjectParser());
        HttpResponse response;
        response = request.execute();
        return  response.parseAs(InitUserProxy.class);
    }
}
