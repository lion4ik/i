package org.alexey.ifree.network.request;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

import org.alexey.ifree.Account;
import org.alexey.ifree.App;
import org.alexey.ifree.network.proxywrapper.SubscribeResultProxy;

/**
 * Created by alexey on 31.12.15.
 */
public class SubscribePush extends GoogleHttpClientSpiceRequest<SubscribeResultProxy> {
    private String device_token;

    public SubscribePush(String device_token) {
        super(SubscribeResultProxy.class);
        this.device_token = device_token;
        setRetryPolicy(null);
    }

    @Override
    public SubscribeResultProxy loadDataFromNetwork() throws Exception {
        GenericUrl url = new GenericUrl("http://XN--80AAAH7AN2E2D.XN--P1AI");
        JacksonFactory factory = new JacksonFactory();
        url.setRawPath("/subscribepush.php");
        url.put("vk_user_id", App.getAccount().getUserId());
        url.put("device_token",device_token);
        HttpRequest request = getHttpRequestFactory()
                .buildGetRequest(url);
        request.setParser(factory.createJsonObjectParser());
        HttpResponse response;
        response = request.execute();
        return  response.parseAs(SubscribeResultProxy.class);
    }
}
