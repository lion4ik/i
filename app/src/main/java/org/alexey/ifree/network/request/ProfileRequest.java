package org.alexey.ifree.network.request;

import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

import org.alexey.ifree.App;
import org.alexey.ifree.network.proxywrapper.ProfileProxy;

import java.util.ArrayList;

/**
 * Created by alexey on 20.12.15.
 */
public class ProfileRequest extends GoogleHttpClientSpiceRequest<ProfileProxy> {
    private long mUid;

    public ProfileRequest(long uid) {
        super(ProfileProxy.class);
        mUid = uid;
        setRetryPolicy(null);
    }

    @Override
    public ProfileProxy loadDataFromNetwork() throws Exception {
        ArrayList<Long> uids = new ArrayList<Long>();
        uids.add(mUid);
        ProfileProxy profileProxy = new ProfileProxy(App.getVKApi().getProfiles(uids,null,"photo_200,counters,city","nom",null,null));
        return profileProxy;
    }
}
