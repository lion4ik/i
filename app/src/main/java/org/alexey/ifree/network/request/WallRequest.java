package org.alexey.ifree.network.request;

import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;
import com.perm.kate.api.WallMessage;

import org.alexey.ifree.App;
import org.alexey.ifree.network.proxywrapper.WallProxy;

/**
 * Created by alexey on 15.12.15.
 */
public class WallRequest extends GoogleHttpClientSpiceRequest<WallProxy> {
    private int mCount;
    private int mOffset;
    private long mId;

    public WallRequest(int count, int offset, long id) {
        super(WallProxy.class);
        mCount = count;
        mOffset = offset;
        mId = id;
        setRetryPolicy(null);
    }

    @Override
    public WallProxy loadDataFromNetwork() throws Exception {
        WallProxy wallProxy = new WallProxy(App.getVKApi().getWallMessages(mId,mCount,mOffset,"all"));
        return wallProxy;
    }
}
