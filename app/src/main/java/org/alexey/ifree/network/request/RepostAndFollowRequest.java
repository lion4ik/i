package org.alexey.ifree.network.request;

import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

import org.alexey.ifree.App;
import org.alexey.ifree.network.proxywrapper.RepostAndFollowProxy;

import java.util.HashMap;

/**
 * Created by alexey on 20.12.15.
 */
public class RepostAndFollowRequest extends GoogleHttpClientSpiceRequest<RepostAndFollowProxy> {
    private long mGroupId;
    private String mMessage;
    private String mObject;

    public RepostAndFollowRequest(long group_id, String message, String object) {
        super(RepostAndFollowProxy.class);
        mGroupId = group_id;
        mMessage = message;
        mObject = object;
        setRetryPolicy(null);
    }

    @Override
    public RepostAndFollowProxy loadDataFromNetwork() throws Exception {
        HashMap<String,Object> res = App.getVKApi().repostAndFollow(mGroupId,mMessage,mObject);
        RepostAndFollowProxy proxy = new RepostAndFollowProxy(((Integer)res.get("success")),((Integer)res.get("repost_count")));
        return proxy;
    }
}


