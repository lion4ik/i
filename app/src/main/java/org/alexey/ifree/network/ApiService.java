package org.alexey.ifree.network;

import com.octo.android.robospice.JacksonGoogleHttpClientSpiceService;

/**
 * Created by alexey on 15.12.15.
 */
public class ApiService extends JacksonGoogleHttpClientSpiceService{

    @Override
    public int getThreadCount() {
        return 2;
    }
}
