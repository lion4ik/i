package org.alexey.ifree.network.request;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

import org.alexey.ifree.model.Winner;
import org.alexey.ifree.network.proxywrapper.WinnersProxy;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alexey on 17.12.15.
 */
public class WinnersRequest extends GoogleHttpClientSpiceRequest<WinnersProxy> {
    public WinnersRequest() {
        super(WinnersProxy.class);
        setRetryPolicy(null);
    }

    @Override
    public WinnersProxy loadDataFromNetwork() throws Exception {
        GenericUrl url = new GenericUrl("http://XN--80AAAH7AN2E2D.XN--P1AI");
        url.setRawPath("/post.php");
        url.put("method", "getWinnersList");
        HttpRequest request = getHttpRequestFactory()
                .buildGetRequest(url)
                .setHeaders(new HttpHeaders()
                                .set("MOBILE", "1")
                );

        HttpResponse response;
        response = request.execute();
        GsonBuilder gsonb = new GsonBuilder();
        Gson gson = gsonb.create();
        Type listType = new TypeToken<List<Winner>>(){}.getType();
        List<Winner> winners = (List<Winner>) gson.fromJson(response.parseAsString(), listType);
        WinnersProxy wProxy = new WinnersProxy((ArrayList<Winner>) winners);
        return wProxy;
    }
}
