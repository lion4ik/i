package org.alexey.ifree.network.request;

import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

import org.alexey.ifree.App;
import org.alexey.ifree.network.proxywrapper.GroupMembersProxy;

import java.util.ArrayList;

/**
 * Created by alexey on 21.12.15.
 */
public class GroupMembersRequest extends GoogleHttpClientSpiceRequest<GroupMembersProxy> {
    public GroupMembersRequest() {
        super(GroupMembersProxy.class);
        setRetryPolicy(null);
    }

    @Override
    public GroupMembersProxy loadDataFromNetwork() throws Exception {
        ArrayList<Long> uids = new ArrayList<Long>();
        uids.add(105166012L);
        GroupMembersProxy groupsProxy = new GroupMembersProxy(App.getVKApi().getGroups(uids,null,"members_count"));
        return groupsProxy;
    }
}
