package org.alexey.ifree.network.proxywrapper;

import org.alexey.ifree.model.Winner;

import java.util.ArrayList;

/**
 * Created by alexey on 17.12.15.
 */
public class WinnersProxy {

    private ArrayList<Winner> mItems;

    public WinnersProxy(ArrayList<Winner> items){
        mItems = items;
    }

    public ArrayList<Winner> getWinners(){
        return mItems;
    }
}
