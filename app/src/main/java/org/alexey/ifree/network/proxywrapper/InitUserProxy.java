package org.alexey.ifree.network.proxywrapper;

import com.google.api.client.util.Key;

/**
 * Created by alexey on 20.12.15.
 */
public class InitUserProxy {

    @Key
    private Integer status;

    @Key
    private String json;

    @Key
    private String invalid_json;

}
