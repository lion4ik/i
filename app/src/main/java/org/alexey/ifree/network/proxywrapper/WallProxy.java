package org.alexey.ifree.network.proxywrapper;

import com.perm.kate.api.WallMessage;

import java.util.ArrayList;

/**
 * Created by alexey on 16.12.15.
 */
public class WallProxy {

    private ArrayList<WallMessage> mItems;

    public WallProxy(ArrayList<WallMessage> items){
        mItems = items;
    }

    public ArrayList<WallMessage> getItems(){
        return mItems;
    }
}
