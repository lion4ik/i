package org.alexey.ifree;

import android.app.Application;
import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.perm.kate.api.Api;


/**
 * Created by alexey on 15.12.15.
 */
public class App extends Application {
    public static final int REQUEST_LOGIN = 1;
    private static Api mVkApi;
    private static Account mAccount;
    private static RequestManager mGlidemanager;
    public static String APP_ID = "5116104";
    public static int GROUP_ID = 105166012;
    private static Context mContext;
    private GoogleAnalytics tracker;

    private Tracker mTracker;

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(getString(R.string.tracker_id));
        }
        return mTracker;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        mVkApi = new Api(null,APP_ID);
        mAccount=new Account();
        mAccount.restore();
        mGlidemanager = Glide.with(mContext);
    }

    public static void setAccessTokenParams(String access_token){
        mVkApi.setAccessToken(access_token);
    }

    public static Api getVKApi()
    {
        return mVkApi;
    }

    public static RequestManager getGlideManager(){
        return mGlidemanager;
    }

    public static Context getAppContext(){
        return mContext;
    }

    public static Account getAccount()
    {
        return mAccount;
    }

}
