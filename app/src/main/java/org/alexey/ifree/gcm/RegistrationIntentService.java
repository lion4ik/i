package org.alexey.ifree.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.alexey.ifree.R;
import org.alexey.ifree.network.ApiService;
import org.alexey.ifree.network.proxywrapper.SubscribeResultProxy;
import org.alexey.ifree.network.request.SubscribePush;

import java.io.IOException;


/**
 * Created by alexey on 19.11.15.
 */

public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";
    private static final String[] TOPICS = {"global"};
    private boolean result;
    private SpiceManager spiceManager2 = new SpiceManager(ApiService.class);

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    public void onDestroy() {
        spiceManager2.shouldStop();
        super.onDestroy();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        spiceManager2.start(this);
    }

    //прописанна в манифесте, получает token и в функции
    // sendReqistartion реализуем передачу токена на сервер
    @Override
    protected void onHandleIntent(Intent intent) {


//        if (App.getSettings().getBoolean("SENT_TOKEN_TO_SERVER", false)) {
            try {
                // [START register_for_gcm]
                // Initially this call goes out to the network to retrieve the token, subsequent calls
                // are local.
                // R.string.gcm_defaultSenderId (the Sender ID) is typically derived from google-services.json.
                // See https://developers.google.com/cloud-messaging/android/start for details on this file.
                // [START get_token]
                InstanceID instanceID = InstanceID.getInstance(this);
                String token = instanceID.getToken(getString(R.string.gcmSenderId),
                        GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                // [END get_token]
                Log.i(TAG, "GCM Registration Token:------------- " + token);

                // TODO: Implement this method to send any registration to your app's servers.
                sendRegistrationToServer(token);

                // Subscribe to topic channels
                subscribeTopics(token);

            } catch (Exception e) {
                Log.d(TAG, "Failed to complete token refresh", e);
            }
    }

    /**
     * Persist registration to third-party servers.
     * <p/>
     * Modify this method to associate the user's GCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */

    private RequestListener<SubscribeResultProxy> mSubscribePushListener = new RequestListener<SubscribeResultProxy>() {
        @Override
        public void onRequestFailure(SpiceException spiceException) {

        }

        @Override
        public void onRequestSuccess(SubscribeResultProxy subscribeResultProxy) {

        }
    };

    private void sendRegistrationToServer(String token) {
        SubscribePush subscribePush = new SubscribePush(token);
        spiceManager2.execute(subscribePush, mSubscribePushListener);
    }

    /**
     * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
     *
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */
    // [START subscribe_topics]
    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }
}