package org.alexey.ifree.viewmodel;

import android.databinding.BaseObservable;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.analytics.HitBuilders;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.perm.kate.api.Attachment;
import com.perm.kate.api.Group;
import com.perm.kate.api.WallMessage;

import org.alexey.ifree.R;
import org.alexey.ifree.network.proxywrapper.RepostAndFollowProxy;
import org.alexey.ifree.network.request.RepostAndFollowRequest;
import org.alexey.ifree.ui.activity.BaseSpicedActivity;
import org.alexey.ifree.ui.activity.MainActivity;
import org.apache.commons.io.output.ProxyOutputStream;

import java.util.ArrayList;

/**
 * Created by alexey on 13.12.15.
 */
public class PostViewModel extends BaseObservable {
    private WallMessage mPost;
    private long mSubscribeToGroup = 0;
    private View.OnClickListener onClick;

    public PostViewModel(WallMessage post){
        mPost = post;
        mSubscribeToGroup = getSubscribeId();
        onClick = new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if(!getIsReposted()){
                    ((MainActivity)view.getContext()).getTracker().send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("Репост розыгрыша")
                            .build());
                    RepostAndFollowRequest repAndFol = new RepostAndFollowRequest(mSubscribeToGroup,
                            view.getContext().getString(R.string.repost_message),
                            "wall-105166012_".concat(String.valueOf(mPost.id)));
                    ((BaseSpicedActivity)view.getContext()).getSpiceManager().execute(repAndFol, new RequestListener<RepostAndFollowProxy>() {
                        @Override
                        public void onRequestFailure(SpiceException spiceException) {

                        }

                        @Override
                        public void onRequestSuccess(RepostAndFollowProxy repostAndFollowProxy) {
                            if(repostAndFollowProxy.isSuccess()>0){
                                mPost.user_reposted = true;
                                view.setBackgroundResource(R.color.colorPrimaryDark);
                                ((Button)view).setText(view.getContext().getString(R.string.btn_already_reposted));
                            }
                        }
                    });
                }
            }
        };
    }

    public String getImageUrl(){
        ArrayList<Attachment> attachments = mPost.attachments;
        for(int i = 0; i < attachments.size(); i++)
        {
            if(attachments.get(i).type.equals("photo")) {
                if(attachments.get(i).photo.src_big != null)
                    return attachments.get(i).photo.src_big;
                if(attachments.get(i).photo.src_xbig != null)
                    return attachments.get(i).photo.src_xbig;
                if(attachments.get(i).photo.src_xxbig != null)
                    return attachments.get(i).photo.src_xxbig;
                if(attachments.get(i).photo.src_xxxbig != null)
                    return attachments.get(i).photo.src_xxxbig;
                if(attachments.get(i).photo.src != null)
                    return attachments.get(i).photo.src;
                if(attachments.get(i).photo.src_small != null)
                    return attachments.get(i).photo.src_small;
            }
        }
        return null;
    }

    public String getText(){
        return mPost.text;
    }

    public boolean getShouldSubscribe(){
        return mSubscribeToGroup != 0;
    }

    public long getSubscribeId(){
        int startInd = mPost.text.indexOf("[");
        if(startInd >= 0) {
            int endInd = mPost.text.indexOf("]");
            String substr = mPost.text.substring(startInd, endInd);
            String[] splited = substr.split("\\|");
            String clubId = splited[0].replace("club", "").replace("[", "");
            mPost.text = mPost.text.replace(substr,splited[1].replace("]","")).replace("]","");
            return Long.valueOf(clubId);
        }
        return 0;
    }

    public boolean getIsReposted(){
        return mPost.user_reposted;
    }

    public String getRepostCountText(){
        return String.format("Репостов %d",mPost.reposts_count);
    }

    public View.OnClickListener getOnClick(){
        return onClick;
    }
}
