package org.alexey.ifree.viewmodel;

import android.databinding.BaseObservable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Toast;

import org.alexey.ifree.R;
import org.alexey.ifree.Utils;
import org.alexey.ifree.model.ProfileItem;

/**
 * Created by alexey on 20.12.15.
 */
public class ProfileItemViewModel extends BaseObservable {
    private ProfileItem mProfileItem;
    private View.OnClickListener onClickListener;

    public ProfileItemViewModel(ProfileItem profileItem){
        mProfileItem = profileItem;
        if(mProfileItem.getTitle().equals("Статус")){
            onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Utils.createSimpleDialog("Информация",view.getContext().getString(R.string.status_info) ,view.getContext()).show();
                }
            };
        }
    }

    public String getText(){
        return mProfileItem.getText();
    }

    public Drawable getIconRes(){
        return mProfileItem.getIconRes();
    }

    public String getTitle(){
        return mProfileItem.getTitle();
    }

    public View.OnClickListener getOnClick(){
        return onClickListener;
    }

}
