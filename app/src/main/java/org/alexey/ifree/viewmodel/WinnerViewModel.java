package org.alexey.ifree.viewmodel;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import org.alexey.ifree.model.Winner;

/**
 * Created by alexey on 17.12.15.
 */
public class WinnerViewModel extends BaseObservable {
    private Winner mWinner;

    private OnClickListener onClick = new OnClickListener(){

        @Override
        public void onClick(View view) {
            try {
                view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("vkontakte://profile/%d", mWinner.getVkUserId()))));
            }
            catch(ActivityNotFoundException e){
                Toast.makeText(view.getContext(), "Приложение ВК не установлено!", Toast.LENGTH_SHORT).show();
            }
        }
    };

    public OnClickListener getOnClick(){
        return onClick;
    }

    public WinnerViewModel(Winner winner){
        mWinner = winner;
    }

    public String getText(){
        String pattern = "%s %s выиграл(а) %s от %s %s";
        String[] splitedDate = getDate().split("-");
        String date = String.format("%s.%s.%s",splitedDate[2],splitedDate[1],splitedDate[0]);
        return String.format(pattern,getFirstName(),getLastName(), getPrizeName(), getBussinesName(), date);
    }

    public String getPostId() {
        return mWinner.getPostId();
    }

    public String getCity() {
        return mWinner.getCity();
    }

    public String getPhotoUrl() {
        return mWinner.getPhotoUrl();
    }

    public String getFirstName() {
        return mWinner.getFirstName();
    }

    public Integer getVkUserId() {
        return mWinner.getVkUserId();
    }

    public String getLastName() {
        return mWinner.getLastName();
    }

    public String getPrizeName() {
        return mWinner.getPrizeName();
    }

    public String getBussinesName() {
        return mWinner.getBussinesName();
    }

    public String getDate() {
        return mWinner.getDate();
    }


}


