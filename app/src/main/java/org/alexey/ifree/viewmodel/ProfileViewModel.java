package org.alexey.ifree.viewmodel;

import android.databinding.BaseObservable;

import com.perm.kate.api.User;

import org.alexey.ifree.Account;

/**
 * Created by alexey on 20.12.15.
 */
public class ProfileViewModel extends BaseObservable {
    private Account mProfile;

    public ProfileViewModel(Account profile){
        mProfile = profile;
    }

    public String getFirstName(){
        return mProfile.getFirstName();
    }

    public String getLastName(){
        return mProfile.getLastName();
    }

    public String getFriendsText(){
        return String.format("%d друзей", mProfile.getFriendsCount());
    }

    public String getPhotoUrl(){
        return mProfile.getPhotoUrl();
    }
}
