package org.alexey.ifree;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by alexey on 16.12.15.
 */
public class Account {
    private String access_token;
    private String photo_url;
    private String first_name;
    private String last_name;
    private String city_title;
    private int friends_count;
    private long user_id;

    public void save(String access_token, long user_id, String photoUrl, String first_name, String last_name, String city_title, int friends_count){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(App.getAppContext());
        SharedPreferences.Editor editor=prefs.edit();
        editor.putString("access_token", access_token);
        editor.putString("photo_url", photoUrl);
        editor.putString("first_name", first_name);
        editor.putString("last_name", last_name);
        editor.putString("city_title", city_title);
        editor.putInt("friends_count", friends_count);
        editor.putLong("user_id", user_id);
        editor.commit();
        this.user_id = user_id;
        this.access_token = access_token;
        this.photo_url = photoUrl;
        this.first_name = first_name;
        this.last_name = last_name;
        this.city_title = city_title;
        this.friends_count = friends_count;
    }

    public String getAccessToken()
    {
        return access_token;
    }

    public String getPhotoUrl(){
        return photo_url;
    }

    public long getUserId()
    {
        return user_id;
    }

    public boolean isAuthorized()
    {
        return user_id!=0 && access_token!=null && !access_token.isEmpty();
    }

    public void logout()
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(App.getAppContext());
        prefs.edit().clear().commit();
        access_token = null;
        user_id = 0;
    }

    public String getRank(){
        if(friends_count < 50)
            return "";
        if(friends_count >= 50 && friends_count < 100)
            return "Оптимист";
        if(friends_count >= 100 && friends_count < 500)
            return "Популярный";
        if(friends_count >= 500)
            return "Звезда";
        return "";
    }

    public void restore(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(App.getAppContext());
        try
        {
            access_token = prefs.getString("access_token", null);
            photo_url = prefs.getString("photo_url", null);
            first_name = prefs.getString("first_name",null);
            last_name = prefs.getString("last_name",null);
            city_title = prefs.getString("city_title",null);
            friends_count = prefs.getInt("friends_count",-1);
            user_id=prefs.getLong("user_id", 0);
        }
        catch(Exception e)
        {
            logout();
        }
    }

    public String getLastName() {
        return last_name;
    }

    public String getFirstName() {
        return first_name;
    }

    public int getFriendsCount() {
        return friends_count;
    }

    public String getCity(){
        return city_title;
    }
}
