package org.alexey.ifree.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.alexey.ifree.App;
import org.alexey.ifree.R;
import org.alexey.ifree.network.proxywrapper.WallProxy;
import org.alexey.ifree.network.request.WallRequest;
import org.alexey.ifree.ui.activity.MainActivity;
import org.alexey.ifree.ui.adapter.PostAdapter;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.FadeInAnimator;

/**
 * Created by alexey on 14.12.15.
 */
public class PostFragment extends SpicedFragment {
    private RecyclerView mListPosts;
    private PostAdapter mAdapter;

    public static PostFragment newInstance(){
        PostFragment f  = new PostFragment();
        return f;
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_post, container, false);
        final SwipeRefreshLayout swipeRefrsh = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mListPosts = (RecyclerView) rootView.findViewById(R.id.recycler_post);
        mListPosts.setLayoutManager(new LinearLayoutManager(getActivity()));
        mListPosts.setHasFixedSize(true);
        mListPosts.setItemAnimator(new FadeInAnimator());
//        mPostAdapter.setItems(mStories);
//        mListPosts.setAdapter(mPostAdapter);
        WallRequest wReq = new WallRequest(100,0,-App.GROUP_ID);
        getSpiceManager().execute(wReq, new RequestListener<WallProxy>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {

            }

            @Override
            public void onRequestSuccess(WallProxy wallProxy) {
                mAdapter = new PostAdapter(wallProxy.getItems());
                AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(mAdapter);
                mListPosts.setAdapter(alphaAdapter);
            }
        });

        swipeRefrsh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                WallRequest wReq = new WallRequest(100,0,-App.GROUP_ID);
                getSpiceManager().execute(wReq, new RequestListener<WallProxy>() {
                    @Override
                    public void onRequestFailure(SpiceException spiceException) {
                        swipeRefrsh.setRefreshing(false);
                    }

                    @Override
                    public void onRequestSuccess(WallProxy wallProxy) {
                        swipeRefrsh.setRefreshing(false);
                        mAdapter.setItems(wallProxy.getItems());
                    }
                });
            }
        });
        return rootView;
    }

    public PostFragment(){}
}
