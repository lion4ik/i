package org.alexey.ifree.ui.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.alexey.ifree.App;
import org.alexey.ifree.R;
import org.alexey.ifree.databinding.ActivityProfileBinding;
import org.alexey.ifree.model.ProfileItem;
import org.alexey.ifree.ui.adapter.ProfileItemAdapter;
import org.alexey.ifree.viewmodel.ProfileViewModel;

import java.util.ArrayList;

public class ProfileActivity extends AppCompatActivity {

    private Tracker mTracker;

    @Override
    protected void onStart() {
        super.onStart();
        mTracker.setScreenName("Экран профиль (android)");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityProfileBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);
        binding.setAccount(new ProfileViewModel(App.getAccount()));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        App application = (App) getApplication();
        mTracker = application.getDefaultTracker();

        Drawable starDrawable = null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            starDrawable =  getResources().getDrawable(R.drawable.ic_star_black_24dp, getTheme());
        } else {
            starDrawable =  getResources().getDrawable(R.drawable.ic_star_black_24dp);
        }
        ArrayList<ProfileItem> profileItems = new ArrayList<ProfileItem>();
        profileItems.add(new ProfileItem(App.getAccount().getRank(),"Статус",starDrawable));
        profileItems.add(new ProfileItem(App.getAccount().getCity(),"Город",null));
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_profile);
        ProfileItemAdapter adapter = new ProfileItemAdapter(profileItems);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("vkontakte://profile/%d", App.getAccount().getUserId()))));
                }
                catch(ActivityNotFoundException e){
                    Toast.makeText(view.getContext(), "Приложение ВК не установлено!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
