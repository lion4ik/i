package org.alexey.ifree.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.alexey.ifree.R;
import org.alexey.ifree.network.proxywrapper.WinnersProxy;
import org.alexey.ifree.network.request.WinnersRequest;
import org.alexey.ifree.ui.adapter.WinnerAdapter;

/**
 * Created by alexey on 17.12.15.
 */
public class WinnersFragment extends SpicedFragment{
    private WinnerAdapter mAdapter;
    private RecyclerView mListWinners;

    public static WinnersFragment newInstance() {
        WinnersFragment fragment = new WinnersFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_post, container, false);
        mListWinners = (RecyclerView) rootView.findViewById(R.id.recycler_post);
        final SwipeRefreshLayout swipeRefresher = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mListWinners.setLayoutManager(new LinearLayoutManager(getActivity()));
        mListWinners.setHasFixedSize(true);
        WinnersRequest wReq = new WinnersRequest();
        getSpiceManager().execute(wReq, new RequestListener<WinnersProxy>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {

            }

            @Override
            public void onRequestSuccess(WinnersProxy winnersProxy) {
                mAdapter = new WinnerAdapter(winnersProxy.getWinners());
                mListWinners.setAdapter(mAdapter);
            }
        });

        swipeRefresher.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                WinnersRequest wReq = new WinnersRequest();
                getSpiceManager().execute(wReq, new RequestListener<WinnersProxy>() {
                    @Override
                    public void onRequestFailure(SpiceException spiceException) {
                        swipeRefresher.setRefreshing(false);
                    }

                    @Override
                    public void onRequestSuccess(WinnersProxy winnersProxy) {
                        swipeRefresher.setRefreshing(false);
                        mAdapter.setItems(winnersProxy.getWinners());
                    }
                });
            }
        });

        return rootView;
    }
}
