package org.alexey.ifree.ui.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.perm.kate.api.WallMessage;

import org.alexey.ifree.R;
import org.alexey.ifree.databinding.ItemPostBinding;
import org.alexey.ifree.viewmodel.PostViewModel;

import java.util.List;

/**
 * Created by alexey on 15.12.15.
 */
public class PostAdapter extends RecyclerView.Adapter<PostAdapter.BindingHolder> {
    private List<WallMessage> mPosts;

    public PostAdapter(List<WallMessage> posts){
        mPosts = posts;
    }

    public void setItems(List<WallMessage> posts){
        mPosts = posts;
        notifyDataSetChanged();
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemPostBinding postBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_post,
                parent,
                false);
        return new BindingHolder(postBinding);
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        ItemPostBinding postBinding = holder.binding;
        PostViewModel viewModel = new PostViewModel(mPosts.get(position));
        postBinding.setViewModel(viewModel);
        postBinding.btnRepostCount.setText(viewModel.getRepostCountText());
    }

    @Override
    public int getItemCount() {
        return mPosts.size();
    }

    public static class BindingHolder extends RecyclerView.ViewHolder {
        private ItemPostBinding binding;

        public BindingHolder(ItemPostBinding binding) {
            super(binding.cardView);
            this.binding = binding;

        }
    }
}
