package org.alexey.ifree.ui.fragment;

import android.support.v4.app.Fragment;

import com.octo.android.robospice.SpiceManager;

import org.alexey.ifree.network.ApiService;

/**
 * Created by alexey on 16.12.15.
 */
public class SpicedFragment extends Fragment {
    private SpiceManager mSpiceManager = new SpiceManager(ApiService.class);


    protected SpiceManager getSpiceManager() {
        return mSpiceManager;
    }


    @Override
    public void onStart() {
        super.onStart();
        getSpiceManager().start(getActivity());
    }

    @Override
    public void onStop() {
        if (getSpiceManager().isStarted()) {
            getSpiceManager().shouldStop();
        }
        super.onStop();
    }
}
