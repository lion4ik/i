package org.alexey.ifree.ui.activity;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.perm.kate.api.User;

import org.alexey.ifree.App;
import org.alexey.ifree.R;
import org.alexey.ifree.Utils;
import org.alexey.ifree.gcm.RegistrationIntentService;
import org.alexey.ifree.network.proxywrapper.InitUserProxy;
import org.alexey.ifree.network.proxywrapper.ProfileProxy;
import org.alexey.ifree.network.request.GroupJoinRequest;
import org.alexey.ifree.network.request.InitUserRequest;
import org.alexey.ifree.network.request.ProfileRequest;
import org.alexey.ifree.ui.fragment.InfoFragment;
import org.alexey.ifree.ui.fragment.PostFragment;
import org.alexey.ifree.ui.fragment.WinnersFragment;

import java.util.HashMap;

import me.drakeet.materialdialog.MaterialDialog;

public class MainActivity extends BaseSpicedActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;


    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private TabLayout tabLayout;
    private Tracker mTracker;

    public Tracker getTracker(){
        return mTracker;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        App application = (App) getApplication();
        mTracker = application.getDefaultTracker();


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(R.string.pager_title_posts);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        mTracker.setScreenName("Экран Наград (android)");
                        setTitle(R.string.pager_title_posts);
                        break;
                    case 1:
                        mTracker.setScreenName("Экран Победителей (android)");
                        setTitle(R.string.pager_title_winners);
                        break;
                    case 2:
                        mTracker.setScreenName("Экран Инфо (Инфо)");
                        setTitle(R.string.pager_title_info);
                        break;
                }
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        if(!App.getAccount().isAuthorized())
        {
            startLoginActivity();
        }
        else {
            App.getAccount().restore();
            App.setAccessTokenParams(App.getAccount().getAccessToken());
            mViewPager.setAdapter(mSectionsPagerAdapter);
            tabLayout.setupWithViewPager(mViewPager);
            setupTabIcons();

            showStyledProgressDialog("Загрузка", "Подождите, пожалуйста...");
            ProfileRequest profReq = new ProfileRequest(App.getAccount().getUserId());
            getSpiceManager().execute(profReq, new RequestListener<ProfileProxy>() {
                @Override
                public void onRequestFailure(SpiceException spiceException) {
                    closeStyledDialog();
                }

                @Override
                public void onRequestSuccess(ProfileProxy profileProxy) {
                    closeStyledDialog();
                    initUserRequest(profileProxy.getItems().get(0));
                }
            });

            GroupJoinRequest grJoin = new GroupJoinRequest(App.GROUP_ID);
            getSpiceManager().execute(grJoin, new RequestListener<String>() {
                @Override
                public void onRequestFailure(SpiceException spiceException) {

                }

                @Override
                public void onRequestSuccess(String s) {

                }
            });
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        mTracker.setScreenName("Главный экран (запуск приложения)");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("DEBUG", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private void setupTabIcons(){
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_present);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_winners);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_info);
    }

    private void startLoginActivity() {
        Intent intent = new Intent();
        intent.setClass(this, LoginActivity.class);
        startActivityForResult(intent, App.REQUEST_LOGIN);
    }

    public void initUserRequest(User currentUser){
        HashMap<String,String> params = new HashMap<String, String>(9);
        params.put("vk_user_id",String.valueOf(currentUser.uid));
        params.put("vk_access_token",App.getAccount().getAccessToken());
        params.put("can_see_wall",String.valueOf(currentUser.can_post));
        params.put("friends_count",String.valueOf(currentUser.friends_count));
        params.put("vk_name",String.valueOf(currentUser.first_name));
        params.put("vk_last_name",String.valueOf(currentUser.last_name));
        params.put("vk_city",String.valueOf(currentUser.city_title));
        params.put("vk_photo",String.valueOf(currentUser.photo_200));
        params.put("device_id","android_empty");
        params.put("device_type","android");

        showStyledProgressDialog("Загрузка", "Подождите, пожалуйста...");
        InitUserRequest initUser = new InitUserRequest(params);
        getSpiceManager().execute(initUser, new RequestListener<InitUserProxy>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {
                closeStyledDialog();
            }

            @Override
            public void onRequestSuccess(InitUserProxy initUserProxy) {
                closeStyledDialog();
                if(checkPlayServices()) {
                    startService(new Intent(MainActivity.this, RegistrationIntentService.class));
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case App.REQUEST_LOGIN:
                if(resultCode == RESULT_OK) {
                    App.setAccessTokenParams(data.getStringExtra("token"));
                    showStyledProgressDialog("Загрузка", "Подождите, пожалуйста...");
                    ProfileRequest profReq = new ProfileRequest(data.getLongExtra("user_id", 0));
                    getSpiceManager().execute(profReq, new RequestListener<ProfileProxy>() {
                        @Override
                        public void onRequestFailure(SpiceException spiceException) {
                            closeStyledDialog();
                        }

                        @Override
                        public void onRequestSuccess(ProfileProxy profileProxy) {
                            closeStyledDialog();
                            User currentUser = profileProxy.getItems().get(0);
                            App.getAccount().save(data.getStringExtra("token"),
                                    data.getLongExtra("user_id", 0),
                                    currentUser.photo_200,
                                    currentUser.first_name,
                                    currentUser.last_name,
                                    currentUser.city_title,
                                    currentUser.friends_count);
                            App.setAccessTokenParams(App.getAccount().getAccessToken());
                            mViewPager.setAdapter(mSectionsPagerAdapter);
                            tabLayout.setupWithViewPager(mViewPager);
                            setupTabIcons();
                            initUserRequest(currentUser);
                            showRulesDialog();

                            GroupJoinRequest grJoin = new GroupJoinRequest(App.GROUP_ID);
                            getSpiceManager().execute(grJoin, new RequestListener<String>() {
                                @Override
                                public void onRequestFailure(SpiceException spiceException) {

                                }

                                @Override
                                public void onRequestSuccess(String s) {

                                }
                            });

                        }
                    });
                }
                else if(resultCode == RESULT_CANCELED)
                    finish();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void showRulesDialog(){
        MaterialDialog dialog = Utils.createSimpleDialog("Правила", "", this);
        LayoutInflater inflater= LayoutInflater.from(this);
        View view=inflater.inflate(R.layout.dialog_rules, null);
        ((TextView)view.findViewById(R.id.tvRules)).setText(getString(R.string.rules));
        dialog.setContentView(view).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id){
            case R.id.menu_profile:
                startActivity(new Intent(this,ProfileActivity.class));
                return true;
            case R.id.menu_rules:
                showRulesDialog();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return PostFragment.newInstance();
                case 1:
                    return WinnersFragment.newInstance();
                case 2:
                    return InfoFragment.newInstance();
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }
}
