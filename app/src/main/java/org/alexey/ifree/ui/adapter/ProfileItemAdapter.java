package org.alexey.ifree.ui.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.alexey.ifree.R;
import org.alexey.ifree.databinding.ItemProfileBinding;
import org.alexey.ifree.model.ProfileItem;
import org.alexey.ifree.viewmodel.ProfileItemViewModel;

import java.util.List;

/**
 * Created by alexey on 20.12.15.
 */
public class ProfileItemAdapter extends RecyclerView.Adapter<ProfileItemAdapter.BindingHolder>{
    private List<ProfileItem> mProfileItems;

    public ProfileItemAdapter(List<ProfileItem> profileItems){
        mProfileItems = profileItems;
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemProfileBinding profileBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_profile,
                parent,
                false);
        return new BindingHolder(profileBinding);
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        ItemProfileBinding binding = holder.binding;
        binding.setProfileItem(new ProfileItemViewModel(mProfileItems.get(position)));
    }

    @Override
    public int getItemCount() {
        return mProfileItems.size();
    }


    public static class BindingHolder extends RecyclerView.ViewHolder{
        private ItemProfileBinding binding;

        public BindingHolder(ItemProfileBinding binding) {
            super(binding.rlProfileItems);
            this.binding = binding;
        }
    }
}
