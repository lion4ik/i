package org.alexey.ifree.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.perm.kate.api.Auth;

import org.alexey.ifree.App;
import org.alexey.ifree.R;

/**
 * Created by alexey on 16.12.15.
 */
public class LoginActivity extends BaseSpicedActivity {
    WebView webview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

            webview = (WebView) findViewById(R.id.vkontakteview);
            WebSettings webSettings = webview.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webSettings.setSaveFormData(false);
            if (Build.VERSION.SDK_INT < 18)
                webSettings.setSavePassword(false);

            webview.clearCache(true);
            webview.setWebViewClient(new VkontakteWebViewClient());

            CookieSyncManager.createInstance(this);

            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();

            String url = Auth.getUrl(App.APP_ID, Auth.getSettings());
            webview.loadUrl(url);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(Activity.RESULT_CANCELED, getIntent());
        finish();
    }

    private class VkontakteWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            parseUrl(url);
            showStyledProgressDialog("Загрузка", "Пожалуйста, подождите...");
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            try {
                closeStyledDialog();
            }
            catch (Exception e){

            }
        }
    }



    private void parseUrl(String url) {
        try {
            if(url==null)
                return;
            if(url.startsWith(Auth.redirect_url))
            {
                if(!url.contains("error=")){
                    String[] auth=Auth.parseRedirectUrl(url);
                    Intent intent=getIntent();
                    long user_id = Long.valueOf(auth[1]).longValue();
                    intent.putExtra("token", auth[0]);
                    intent.putExtra("user_id", user_id);
                    setResult(Activity.RESULT_OK, intent);

                }
                finish();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
