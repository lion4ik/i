package org.alexey.ifree.ui.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.octo.android.robospice.SpiceManager;

import org.alexey.ifree.network.ApiService;

/**
 * Created by alexey on 16.12.15.
 */
public class BaseSpicedActivity extends AppCompatActivity {
    private SpiceManager spiceManager = new SpiceManager(ApiService.class);
    private ProgressDialog mProgressDialog;
    private InputMethodManager inputManager;

    public void hideKeyboard(View v) {
        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public void showKeyboard(View v) {
        inputManager.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        mProgressDialog = new ProgressDialog(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            mProgressDialog.setProgressStyle(android.R.style.Theme_Material_Light_NoActionBar);
        else
            mProgressDialog.setProgressStyle(android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setIndeterminate(true);
    }

    public void showStyledProgressDialog(String title, String message) {
        mProgressDialog.setMessage(message);
        mProgressDialog.setTitle(title);
        mProgressDialog.show();
    }

    public void showStyledProgressDialog(int titleResId, int messageResId) {
        mProgressDialog.setMessage(getString(messageResId));
        mProgressDialog.setTitle(titleResId);
        mProgressDialog.show();
    }

    public void closeStyledDialog() {
        mProgressDialog.dismiss();
    }

    public SpiceManager getSpiceManager() {
        return spiceManager;
    }

    @Override
    protected void onStop() {
        super.onStop();
        spiceManager.shouldStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        spiceManager.start(this);
    }
}