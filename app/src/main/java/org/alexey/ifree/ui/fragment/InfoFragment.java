package org.alexey.ifree.ui.fragment;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.alexey.ifree.R;
import org.alexey.ifree.network.proxywrapper.GroupMembersProxy;
import org.alexey.ifree.network.request.GroupMembersRequest;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link InfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InfoFragment extends SpicedFragment {

    public InfoFragment() {
    }

    public static InfoFragment newInstance() {
        InfoFragment fragment = new InfoFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_info, container, false);
        Button btnContactUs = (Button) rootView.findViewById(R.id.btnContactUs);
        final TextView tvMembersCount = (TextView) rootView.findViewById(R.id.tvUsersInGroup);

        btnContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("vkontakte://profile/2502583")));
                }
                catch(ActivityNotFoundException e){
                    Toast.makeText(view.getContext(), "Приложение ВК не установлено!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        GroupMembersRequest gReq = new GroupMembersRequest();
        getSpiceManager().execute(gReq, new RequestListener<GroupMembersProxy>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {

            }

            @Override
            public void onRequestSuccess(GroupMembersProxy groupMembersProxy) {
                tvMembersCount.setText(String.format("Количество пользователей в группе: %d",groupMembersProxy.getItems().get(0).members_count));
            }
        });

        return rootView;
    }

}
