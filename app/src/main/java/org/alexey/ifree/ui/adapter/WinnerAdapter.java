package org.alexey.ifree.ui.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import org.alexey.ifree.R;
import org.alexey.ifree.databinding.ItemWinnerBinding;
import org.alexey.ifree.model.Winner;
import org.alexey.ifree.viewmodel.WinnerViewModel;

import java.util.List;

/**
 * Created by alexey on 17.12.15.
 */
public class WinnerAdapter extends RecyclerView.Adapter<WinnerAdapter.BindingHolder>{
    private List<Winner> mWinners;

    public WinnerAdapter(List<Winner> winners){
        mWinners = winners;
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemWinnerBinding postBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_winner,
                parent,
                false);
        return new BindingHolder(postBinding);
    }

    public void setItems(List<Winner> winners){
        mWinners = winners;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        ItemWinnerBinding winnerBinding = holder.binding;
        winnerBinding.setWinner(new WinnerViewModel(mWinners.get(position)));
    }

    @Override
    public int getItemCount() {
        return mWinners.size();
    }

    public static class BindingHolder extends RecyclerView.ViewHolder {
        private ItemWinnerBinding binding;

        public BindingHolder(ItemWinnerBinding binding) {
            super(binding.rlWinner);
            this.binding = binding;
        }
    }
}
